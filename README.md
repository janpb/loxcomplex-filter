# README

Low complexity filter based on Shannon entropy.

## Install

`git clone https://gitlab.com/janpb/loxcomplex-filter.git`

## Usage

`src/lowcomplex-filter.py -h`

Filter protein sequences with a min. entropy of 2 and min. metric entropy of
0.02 and store sequences in new file:

`src/lowcomplex-filter.py -f FASTAfile.faa -t p -e 2 -m 0.02 > sequences.faa`

## Output

Sequences meeting the given parameters are written to STDOUT. A summary is
written to STDERR.
