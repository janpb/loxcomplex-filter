#!/usr/bin/env python3
"""
..
  Copyright 2022 Jan P Buchmann

.. moduleauthor:: Jan Piotr Buchmann <jpb@members.fsf.org>
"""

import argparse
import sys
import math

class Sequence:

  stop = '*'

  def __init__(self, seqtype, header=None, sequence=None) -> None:
    self.name = header
    self.seq = ""
    self.seqtyp = seqtype
    self.add_sequence(sequence)

  def header(self):
    return self.name

  def accession(self):
    return self.name.split(' ')[0]

  def sequence(self):
    return self.seq

  def length(self):
    return len(self.seq)

  def add_sequence(self, sequence):
    if sequence:
      self.seq += sequence

class NucleotideSequence(Sequence):

  residues = set(['A', 'C', 'T', 'G'])
  mask = 'N'

  def __init__(self, header=None, sequence=None) -> None:
    super().__init__('nucl', header, sequence)

class ProteinSequence(Sequence):

  residues = set(['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N',
                  'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'Y'])
  mask = 'X'

  def __init__(self, header=None, sequence=None) -> None:
    super().__init__('prot', header, sequence)

class FastaReader:

  header = ['sequence', 'seqtype', 'length', 'entropy', 'metricEntropy',
            'maxEntropy', 'metricEntropyCutoff', 'entropyCutoff', 'keep']

  def __init__(self, seqtype='n', mentropy=0.0, entropy=0.0) -> None:
    self.sequences = {}
    self.seqtype = seqtype
    self.mentropy = mentropy
    self.entropy = entropy

  def read(self, fastafile, nomask=False):
    shc = EntropyCalculator(nomask)
    fh = open(fastafile)
    sequence = None
    keepstat = 'N'
    for i in fh:
      if i[0] == '>' and sequence:
        self.sequences.update({sequence.header:sequence})
        sh = shc.calculate(sequence)
        print('\t'.join(str(x) for x in [sequence.header() , sequence.seqtyp,
                                         sequence.length(), sh.sh, sh.msh,
                                         sh.max, self.mentropy, self.entropy]),
                                         end="\t", file=sys.stderr)
        if sh.msh >= self.mentropy and sh.msh >= self.entropy:
          print(f">{sequence.header()}\n{sequence.sequence()}")
          keepstat = 'Y'
        print(keepstat, file=sys.stderr)
        sequence = self.new_sequence(i.strip()[1:])
      elif i[0] == '>':
        sequence = self.new_sequence(i.strip()[1:])
      else:
        sequence.add_sequence(i.strip())
    fh.close()

  def new_sequence(self, header, sequence=None):
    if self.seqtype == 'p':
      return ProteinSequence(header, sequence)
    return NucleotideSequence(header, sequence)

  def print_header(self):
    print('\t'.join(x for x in FastaReader.header), file=sys.stderr)


class EntropyCalculator:

  class Entropy:
    def __init__(self, sh, msh, maxsh) -> None:
      self.sh = sh
      self.msh = msh
      self.max = maxsh

  def __init__(self, nomask=False) -> None:
    self.nomask = nomask

  def calculate(self, sequence):
    p_res = self.calc_residue_probabilities(sequence)
    shannon_h = 0
    for i in p_res:
      shannon_h += p_res[i] * math.log2(1/p_res[i])
    return EntropyCalculator.Entropy(shannon_h, shannon_h/sequence.length(), self.max_h(sequence))

  def calc_residue_probabilities(self, sequence):
    p_residues = {}
    seqlen = 0
    for i in sequence.sequence():
      if i == sequence.stop:
        continue
      if self.nomask and i == sequence.mask:
        continue
      if i not in p_residues:
        p_residues[i] = 0
      p_residues[i] += 1
      seqlen += 1
    for i in p_residues:
      p_residues[i] /= seqlen
    return p_residues

  def max_h(self, sequence):
    return math.log2(len(sequence.residues))


def main():
  ap = argparse.ArgumentParser(description='Filtering sequences on Shannon entropy')
  ap.add_argument('-f', '--file', help='FASTA file', required=True)
  ap.add_argument('-t', '--type', help='n(ucleotide) or p(rotein)', default='n', required=True)
  ap.add_argument('--nomask', help='ignore unknown residues', action='store_true', default=False)
  ap.add_argument('-m' ,'--mentropy', help='ignore sequences with lower metric entropy', default=0.0, type=float)
  ap.add_argument('-e' ,'--entropy', help='ignore sequences with lower entropy', default=0.0, type=float)
  args = ap.parse_args()
  if not args.file:
    sys.exit(f"Require FASTA input file. Check {ap.prog} -h")
  fr = FastaReader(args.type, args.mentropy, args.entropy)
  fr.print_header()
  fr.read(args.file, args.nomask)
  return 0

if __name__ == '__main__':
  main()
